<?php

namespace Src\Classes;

/**
 * Description of Session
 *
 * @author Serg
 */
class Session {

    public static function setName($name) {
        session_name($name);
    }

    public static function getName($name) {
        return session_name();
    }

    public static function getId() {
        return session_id();
    }

    public static function setId($id) {
        session_id($id);
    }
    
    public static function sessionExists() {
        if (isset($_SESSION)){
            return true;
        } else {
            return false;
        }
    }

    public static function start() {
        return session_start();
    }

    public static function destroy() {
        $_SESSION = array();
        $_COOKIE = array();
        unset($_COOKIE);
        unset($_SESSION);
        return session_destroy();
    }
    
    public static function setSavePath($path) {
        return session_save_path($path);
    }

    public static function set($key, $value) {
        $_SESSION[$key] = $value;
    }

    public static function add($key, $value) {
        array_push($_SESSION[$key], $value);
    }

    public static function get($key) {
        return $_SESSION[$key];
    }
    
    public static function delete($key) {
        unset($_SESSION[$key]);
    }//put your code here
}
