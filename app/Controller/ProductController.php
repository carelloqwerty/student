<?php
/**
 * Created by PhpStorm.
 * User: phpuser
 * Date: 12.07.18
 * Time: 15:26
 */

namespace App\Controller;
use App\Models\Product;

class ProductController
{
    public function actionView($id){
        $category = Product::categories();
        if($id){
            $product = Product::getItemById($id);
        }
        $knopka = isset($_SERVER['HTTP_REFERER'])?
            substr($_SERVER['HTTP_REFERER'], -3)!=substr($_SERVER['REQUEST_URI'],-3)?$_SERVER['HTTP_REFERER']:'../':'../';
        require_once ROOT.'/app/Views/product.php';

        return true;
    }
}