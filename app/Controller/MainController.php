<?php
/**
 * Created by PhpStorm.
 * User: phpuser
 * Date: 12.07.18
 * Time: 15:26
 */

namespace App\Controller;
use App\Models\Product;

class MainController
{
    public function actionAll()
    {
        $productList = [];
        $productList = Product::showAll();
        $category = Product::categories();

        require_once ROOT.'/app/Views/main.php';

        return true;
    }

}