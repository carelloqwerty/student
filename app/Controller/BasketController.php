<?php
/**
 * Created by PhpStorm.
 * User: phpuser
 * Date: 12.07.18
 * Time: 15:27
 */

namespace App\Controller;
use App\Models\Basket;
use App\Models\Product;
use Src\Classes\Session;

class BasketController
{
    public function actionView()
    {   
        $category = Product::categories(); //зачем я их сюда прилепил? Вспомнил, что бы был переход из корзины в категории
        $basket = Basket::getProducts();
                
        if ($basket) {
            // Получаем полную информацию о товарах для списка
            $productsIds = array_keys($basket);        
            $products = Product::getProdustsByIds($productsIds);
            $totalPrice = Basket::getTotalPrice($products);
        }

        require_once(ROOT . '/app/Views/basket.php');
    }

    public function actionAdd($id)
    {

        Basket::addProduct($id);

        $referrer = $_SERVER['HTTP_REFERER'];
        
        header("Location: $referrer");
    }
    public function actionRemove($id)
    {
        Basket::removeProduct($id);

        $referer = $_SERVER['HTTP_REFERER'];
        
        header("Location: $referer");
    }

    public function actionRemoveId($id)
    {
        Basket::removeProductId($id);

        $referer = $_SERVER['HTTP_REFERER'];

        header("Location: $referer");
    }
    public function actionProove(){
        Basket::addOrder();
    //реализовать подтверждение заказа
    }
}