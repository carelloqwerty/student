<?php
/**
 * Created by PhpStorm.
 * User: phpuser
 * Date: 17.07.18
 * Time: 13:10
 */

namespace App\Controller;

use App\Models\User;
use Src\Classes\Session;


class UserController
{
    public function actionReg()
    {

        if (isset($_POST['submit'])) {
            $login = $_POST['login'];
            $password = $_POST['password'];
            $email = $_POST['email'];

            $errors = false;

            if (!User::chekLogin($login)) {
                $errors[] = 'Логин должен состоять минимум из 2 символов';
            }
            if (!User::chekPass($password)) {
                $errors[] = 'Пароль должен состоять минимум из 6 символов';
            }
            if (!User::chekEmail($email)) {
                $errors[] = 'Введите корректный адрес';
            }
            if (User::ifEmailExist($email)) {
                $errors[] = 'адресс существует';

            }

            if ($errors == false) {$login = true;
                $result = User::register($login, $password, $email);
            }
        }

        require_once(ROOT . '/app/Views/registr.php');
        return true;
    }
    
    public function actionLogin(){
        
        if(isset($_POST['submit'])) {
            $logOrMail = $_POST['login'];

            $password = $_POST['password'];
            
            $errors = false;

            $userID = User::checkUserData($logOrMail, $password);

            if($userID == false) {
                $errors[] = 'Неверные данные';
            } else {
                User::auth($userID);
            
            if (isset($_SERVER['HTTP_REFERER'])){
                if (substr($_SERVER['HTTP_REFERER'], 15)=='login' || substr($_SERVER['HTTP_REFERER'], 15)=='registr'){
                    $refer = '../';
                } else {
                    $refer = $_SERVER['HTTP_REFERER'];
                }
            } else {
                $refer = '../';
            }
//то же самое условие через тернарный оператор
//            $refer=isset($_SERVER['HTTP_REFERER'])?
//                substr($_SERVER['HTTP_REFERER'], 15)=='login' || substr($_SERVER['HTTP_REFERER'], 15)=='registr'?'../':
//                    $_SERVER['HTTP_REFERER']:'../';

                header("Location: $refer");
            }
        }
        require_once(ROOT . '/app/Views/login.php');
        return true;
    }

    public function actionLogout(){
        Session::destroy();
        header("Location: /");
    }
    
    public function actionHistory(){

        $history=User::history();

        require_once(ROOT . '/app/Views/history.php');
    }
}