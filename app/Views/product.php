<?php
require_once "header.php";
?>
<div class="row">
    <div class="">
        <div class="thumbnail">
            <div class="thumb boxImg mainImg">
                <img class="" src="<?=$product['image']?>" alt="...">
            </div>

            <div class="caption">
                <h3><?=$product['brend'] . ' ' . $product['name']?></h3>
                <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Details</a></li>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Accessories</a></li>
                        <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Comments</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home" style="margin-top: 20px"><?=$product['specification']?></div>
                        <div role="tabpanel" class="tab-pane" id="profile" style="margin-top: 20px">Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica. Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid. Aliquip placeat salvia cillum iphone. Seitan aliquip quis cardigan american apparel, butcher voluptate nisi qui.</div>
                        <div role="tabpanel" class="tab-pane" id="messages" style="margin-top: 20px">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</div>
                    </div>

                </div>
                <p><a href="/basket/add/<?=$product['id']?>" class="btn btn-primary" role="button" style="margin-top: 20px">Buy for <?=($product['price']/100 . ' грн. ')?></a> <a href="<?=$knopka?>" class="btn btn-default" role="button" style="margin-top: 20px">Go back</a></p>
            </div>
        </div>
    </div>
</div>
<?php

require_once "footer.php";
?>