<?php
require_once "header.php";
?>

<div class="row">
<?php foreach ($productList as $values){?>
    <div class="col-md-4 col-sm-6 boxheight">
        <div class="boxImg">

            <img class="center" src="<?=$values['image'];?>">

        </div>
        <div class="styletext"><?=$values['brend'].' '.$values['name'];?></div>
        <div class="styletext"><?=$values['price']/100;?> грн.</div>
        <div class="style">

            <a type="button" class="btn btn-success left" data-dismiss="modal" href="/basket/add/<?=$values['id']?>">Add</a>
            <a type="button" class="btn btn-success right" role="button" href="/product/<?=$values['id']?>">Detail</a>
        </div>
    </div>
<?php } ?>
</div>
<?php

require_once "footer.php";
?>