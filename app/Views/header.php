<?php Use App\Models\Basket; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <meta charset="UTF-8">
    <title>Index</title>
</head>
<body>
<div class="wrapper">
    <header>
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/main">My Store</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Menu</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false"><?=isset($categoryName)?$categoryName:'Category'?><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <?php foreach ($category as $values){?>
                                    <li><a href="<?='../'.$values['name']?>"><?=$values['name'];?></a></li>
                                <?php }?>
                            </ul>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/basket">Корзина - <?= Basket::countItems();?></a></li>
                        <?php if(isset($_SESSION['user'])){?>
                            <li><a href="/logout">Logout</a><li>
                            <li><a href="/history">History</a><li>
                        <?php ;} else {?>
                        <li><a href="#" data-toggle="modal" data-target="#myModal">Login </a></li>
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <?php if (isset($errors) && is_array($errors)) {
                                        foreach ($errors as $value) {
                                            echo '<br>' . $value;
                                        }
                                    } ?>
                                    <form action = "/login" method="POST">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                &times;
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">Login</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="input-group">
                                                <span class="input-group-addon" name="login">login or email</span>
                                                <input name="login" type="text" class="form-control" placeholder="login or email">
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon" name="password">Password</span>
                                                <input name="password" type="text" class="form-control" placeholder="Password">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть
                                            </button>
                                            <input name="submit" type="submit" class="btn btn-primary" value="Login"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <li><a href="/registr">Registration</a></li>
                        <?php }?>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
