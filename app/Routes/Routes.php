<?php
return [
    'main' => '\App\\Controller\\Main/all',
    'basket/add/([0-9]+)' => '\App\\Controller\\Basket/Add/$1',
    'basket/remove/([0-9]+)' => '\App\\Controller\\Basket/Remove/$1',
    'basket/removeId/([0-9]+)' => '\App\\Controller\\Basket/RemoveId/$1',
    'basket/proove' => '\App\\Controller\\Basket/Proove',
    'basket'=>'\App\\Controller\\Basket/view',
//    'product/([a-z]+)/([0-9]+)' => '\App\\Controller\\Product/view/$1/$2', планироывл поиск по id($2) в категории ($1)
    'product/([0-9]+)' => '\App\\Controller\\Product/view/$1',
    'registr' => '\App\\Controller\\User/reg',
    'login'=>'\App\\Controller\\User/login',
    'logout'=>'\App\\Controller\\User/logout',
    'history' =>'\App\\Controller\\User/history',
    '([a-z]+)' => '\App\\Controller\\Category/CategoryAll/$1',//для фильтра по категориям
    null => '\App\\Controller\\Main/all',

];