<?php

namespace App\Models;
use Src\Classes\Session;
use Src\DbConection\Db;
/**
 * Description of Product
 *
 * @author Serg
 */
class Product {
    public static function getItemById($id){

        $db = Db::getConection();
        $result = $db->query('SELECT * FROM student_db.goods WHERE id='.$id);
        $product = $result->fetch();
        
        return $product;
    }
    public static function showAll(){
        
        $db = Db::getConection();
        $products = [];
        $result = $db->query('SELECT * FROM student_db.goods');
        
        while ($row = $result->fetch()){
            $products[] = $row;
        }
        
        return $products;
    }
    public static function categories(){
        $db = Db::getConection();
        $category = array();
        $result = $db->query('SELECT * FROM student_db.category');

        while ($row = $result->fetch()){
            $category[] = $row;
        }

        return $category;
    }

    public static function getItemByCategory($category){
        $sql = "SELECT `goods`.* FROM student_db.goods AS goods INNER JOIN student_db.category ON category.id=goods.category_id WHERE category.name = '$category'";
        $db = Db::getConection();
        $result = $db->query($sql);
        while ($row = $result->fetch()){
            $products[] = $row;
        }

        return $products;
    }

    public static function getProdustsByIds($idsArray){
        $db = Db::getConection();
        $products = [];
        $price=[];
        $idsString = implode(',', $idsArray);
        $sql = "SELECT * FROM student_db.goods WHERE id IN ($idsString)";
        $result = $db->query($sql);
        while ($row = $result->fetch()){
            $products[] = $row;
            $price[$row['id']] = $row['price'];

        }
        
        $amount=$_SESSION['products'];
        
        foreach ($amount as $key => $value) {
            $price[$key]=[$price[$key],$value];
        }    
        $_SESSION['price']=$price;//нужно будет для подтверждения заказа

        return $products;
    }

}
