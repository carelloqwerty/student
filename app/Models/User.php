<?php
/**
 * Created by PhpStorm.
 * User: phpuser
 * Date: 17.07.18
 * Time: 12:59
 */

namespace App\Models;
use Src\DbConection\Db;
use Src\Classes\Session;
use PDO;

class User
{
    public static function register($login,$password,$email){

        $sql="INSERT INTO student_db.users (login, password, email) VALUES (:login, :password, :email)";
        $db= Db::getConection();
        $result= $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();
    }
    public static function chekLogin($login){
        if (strlen($login)>=2){
            return true;
        }else{
            return false;
        }
    }
    public static function chekPass($pass){
        if (strlen($pass)>=6){
            return true;
        }else{
            return false;
        }
    }
    public static function chekEmail($email){
        if (filter_var($email,FILTER_VALIDATE_EMAIL)){
            return true;
        }else{
            return false;
        }
    }

    public static function ifEmailExist($email){
        $db= Db::getConection();
        $sql = 'SELECT email FROM student_db.users WHERE email = :email';
        $result= $db->prepare($sql);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->execute();
        $check=[];
        while ($row = $result->fetch()){
            $check = $row;
        }
        if ($check){
            return true;
        }else{
            return false;
        }
    }
    
    public static function checkUserData($logOrMail, $password){

        $db = Db::getConection();
        $sql = 'SELECT * FROM users WHERE email = :email or login = :login AND password = :password';
        $result = $db->prepare($sql);
        $result->bindParam(':email', $logOrMail);
        $result->bindParam(':login', $logOrMail);
        $result->bindParam(':password', $password);
        $result->execute();
        $user=[];
        while ($row = $result->fetch()){
            $user = $row;
        }

        if($user){
            return $user['id_users'];
        } else {
            return false;
        }
    }
    
    public static function getUserById($id){
        if($id) {
            $db = Db::getConection();
            $sql = 'SELECT * FROM users WHERE id = :id';
            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);
            return $result->fetch();
        }
    }

    public static function auth($userId){

        $_SESSION['user'] = $userId;
    }
    
    public function history(){
        $sql="SELECT goods.brend, name, goods_order.price, amount, order_id FROM `goods_order` 
            INNER JOIN `goods` ON goods.id = goods_order.goods_id
            INNER JOIN `order` ON `order`.id = goods_order.order_id WHERE `order`.user_id = {$_SESSION['user']} order by order_id desc";
        
        $db = Db::getConection();
        $result = $db->query($sql);
        $history=[];
        while ($row = $result->fetch()){
            $history[$row['order_id']][] = $row;
        }
        
        return $history;
    }
    
}