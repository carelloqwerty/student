<?php
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);


define ('ROOT', dirname (stristr(__FILE__, 'index', true)));

require_once '../vendor/autoload.php';
use Src\Route\Router;
use Src\Classes\Session;

Session::start();
$router = new Router();
$router->run();